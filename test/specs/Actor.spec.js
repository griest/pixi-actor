/* eslint-disable no-unused-expressions */
import Actor from '../../index'

const path = require('path')
const numTextures = 5
const frameTime = 0.2

function rand (max = numTextures) {
  return Math.floor(Math.random() * max)
}

function processClip (clip) {
  return {
    frames: clip.frames,
    next: clip.next,
    speed: clip.speed || 1
  }
}

describe('Actor', function () {
  before('load the textures', function () {
    this.frameObjects = []
    for (let i = 0; i < numTextures; i++) {
      this.frameObjects.push({
        texture: PIXI.Texture.fromImage(path.resolve(`/assets/texture${i}.png`), true), // eslint-disable-line
        time: frameTime
      })
    }
  })

  before('create the clip objects', function () {
    this.clips = [
      {name: '210', frames: [2, 1, 0]},
      {name: '424', frames: [4, 2, 4], next: '210'},
      {name: '13', frames: [1, 3], next: 2, speed: 2}
    ]
  })

  beforeEach('init the Actor with frame objects and clips', function () {
    this.actor = new Actor(this.frameObjects)
    this.actor.addClips(...this.clips)
  })

  beforeEach('pick a random frame and clip', function () {
    this.randomFrame = rand()
    this.randomClip = this.clips[rand(this.clips.length)]
  })

  afterEach('destroy the Actor', function () {
    this.actor.destroy()
  })

  // getters and setters

  describe('#reservedClipName', function () {
    it('should change the name of the reserved clip', function () {
      const clip = this.actor.reservedClip
      const name = 'get schwifty'
      this.actor.reservedClipName = name
      this.actor.clips[name].should.equal(clip)
    })
  })

  describe('#default', function () {
    it('should set the value of default', function () {
      const name = this.randomClip.name
      this.actor.default = name
      this.actor.default.should.eql({clip: name, index: 0})
    })
  })

  describe('#loop', function () {
    it('should remove next value from reserved clip', function () {
      this.actor.loop = false
      this.actor.loop.should.be.false
      should.not.exist(this.actor.reservedClip.next)
    })

    it('should add next value to reserved clip', function () {
      this.actor.loop = true
      this.actor.loop.should.be.true
      this.actor.reservedClip.next.should.equal(this.actor.reservedClipName)
    })
  })

  // class methods

  describe('.constructor', function () {
    it('should store the textures', function () {
      const textures = this.frameObjects.map(e => e.texture)
      const actor = new Actor(textures)

      actor.textures.should.eql(textures)
    })

    it('should navigate to the default frame', function () {
      this.actor.currentFrame.should.equal(0)
      this.actor._texture.should.equal(this.frameObjects[0].texture)
    })
  })

  // instance methods

  describe('#removeClips', function () {
    it('should remove a clip', function () {
      const name = this.randomClip.name
      should.exist(this.actor.clips[name])
      this.actor.removeClips(name)
      should.not.exist(this.actor.clips[name])
    })
  })

  describe('#clipIndex', function () {
    it('should handle a frame arg', function () {
      this.actor.clipIndex(2).should.eql({clip: this.actor.reservedClipName, index: 2})
    })

    it('should handle a clip arg with no index', function () {
      const name = this.randomClip.name
      this.actor.clipIndex(name).should.eql({clip: name, index: 0})
    })

    it('should handle a clip arg with index', function () {
      const name = this.randomClip.name
      this.actor.clipIndex(name, 2).should.eql({clip: name, index: 2})
    })

    it('should handle a clip index arg', function () {
      const ci = {clip: this.randomClip.name, index: 1}
      this.actor.clipIndex(ci).should.eql(ci)
    })
  })

  describe('#nextFrame', function () {
    it('should return the correct frame in clip', function () {
      this.actor.nextFrame('424').should.equal(2)
    })

    it('should return the correct frame in clip with index', function () {
      this.actor.nextFrame('210', 1).should.equal(0)
    })

    it('should return the correct frame in clip with clip index', function () {
      this.actor.nextFrame({clip: '424', index: 1}).should.equal(4)
    })

    it('should return the correct frame from next clip', function () {
      this.actor.nextFrame('424', 2).should.equal(2)
    })

    it('should return the correct frame from next frame', function () {
      this.actor.nextFrame('13', 1).should.equal(2)
    })
  })

  describe('#play', function () {
    it('should start animation', function () {
      this.actor.playing.should.be.false
      this.actor.play()
      this.actor.playing.should.be.true
    })

    it('should do nothing if the actor is already playing', function () {
      this.actor.play()
      this.actor.playing.should.be.true
      this.actor.play()
      this.actor.playing.should.be.true
    })
  })

  describe('#stop', function () {
    it('should stop animation', function () {
      this.actor.play()
      this.actor.playing.should.be.true
      this.actor.stop()
      this.actor.playing.should.be.false
    })
  })

  describe('#goto', function () {
    it('should navigate to the correct frame', function () {
      this.actor.goto(this.randomFrame)
      this.actor.currentFrame.should.equal(this.randomFrame)
    })

    it('should navigate to the correct frame of clip', function () {
      this.actor.goto(this.randomClip.name)
      this.actor.currentFrame.should.equal(this.randomClip.frames[0])
    })

    it('should set the current clip', function () {
      this.actor.goto(this.randomClip.name)
      this.actor._clipIndex.clip.should.equal(this.randomClip.name)
    })

    it('should set the clip index to 0', function () {
      this.actor.goto(this.randomClip.name)
      this.actor._clipIndex.index.should.equal(0)
    })
  })

  describe('#gotoAndPlay', function () {
    it('should call #goto with the correct frame', function () {
      const spy = sinon.spy(this.actor, 'goto')
      this.actor.gotoAndPlay(this.randomFrame)
      spy.should.have.been.calledWith(this.randomFrame)
    })

    it('should call #play', function () {
      const spy = sinon.spy(this.actor, 'play')
      this.actor.gotoAndPlay(this.randomFrame)
      spy.should.have.been.calledOnce
    })
  })

  describe('#gotoAndStop', function () {
    it('should call #goto with the correct frame', function () {
      const spy = sinon.spy(this.actor, 'goto')
      this.actor.gotoAndStop(this.randomFrame)
      spy.should.have.been.calledWith(this.randomFrame)
    })

    it('should call #stop', function () {
      const spy = sinon.spy(this.actor, 'stop')
      this.actor.gotoAndStop(this.randomFrame)
      spy.should.have.been.calledOnce
    })
  })

  describe('#totalFrames', function () {
    it('should return the total number of frames', function () {
      this.actor.totalFrames.should.equal(numTextures)
    })
  })

  describe('#update', function () {
    this.timeout(5000)

    before('define the update function', function () {
      this.update = (frames = 1) => this.actor.update(frames * frameTime)
    })

    // frames

    it('should navigate to the next frame', function (done) {
      this.actor.onFrameChange = ({clipIndex, frame}) => {
        this.actor.currentFrame.should.equal(1)

        this.actor.onFrameChange = ({clipIndex, frame}) => {
          this.actor.currentFrame.should.equal(2)
          done()
        }

        this.update()
      }

      this.actor.gotoAndStop(1)
    })

    it('should pass the correct frame to onFrameChange', function (done) {
      let _frame = 0

      this.actor.onFrameChange = ({frame, clipIndex}) => {
        frame.should.equal(_frame)
        done()
      }

      this.actor.gotoAndStop(_frame)
    })

    it('should loop to frame 0', function (done) {
      this.actor.onFrameChange = ({clipIndex, frame}) => {
        this.actor.currentFrame.should.equal(numTextures - 1)

        this.actor.onFrameChange = ({clipIndex, frame}) => {
          this.actor.currentFrame.should.equal(0)
          done()
        }

        this.update()
      }

      this.actor.gotoAndStop(numTextures - 1)
    })

    it('should call onLoop', function (done) {
      const spy = sinon.spy()

      this.actor.onLoop = spy

      this.actor.onFrameChange = ({clipIndex, frame}) => {
        this.actor.onFrameChange = ({clipIndex, frame}) => {
          spy.should.have.been.calledOnce
          done()
        }

        this.update()
      }

      this.actor.gotoAndStop(numTextures - 1)
    })

    // clips

    it('should navigate to the second frame in the clip', function (done) {
      this.actor.onFrameChange = ({clipIndex, frame}) => {
        this.actor.currentFrame.should.equal(4)

        this.actor.onFrameChange = ({clipIndex, frame}) => {
          this.actor.currentFrame.should.equal(2)
          done()
        }

        this.update()
      }

      this.actor.gotoAndStop('424')
    })

    it('should pass the correct clip to onFrameChange', function (done) {
      const name = this.randomClip.name

      this.actor.onFrameChange = ({clipIndex, frame}) => {
        clipIndex.clip.should.equal(name)
        done()
      }

      this.actor.gotoAndStop(name)
      this.update()
    })

    it('should navigate to the next frame in the clip instead of looping', function (done) {
      this.actor.onFrameChange = ({clipIndex, frame}) => {
        this.actor.currentFrame.should.equal(4)
        this.actor._clipIndex.index.should.equal(0)

        this.actor.onFrameChange = ({clipIndex, frame}) => {
          this.actor.currentFrame.should.equal(2)
          this.actor._clipIndex.index.should.equal(1)
          done()
        }

        this.update()
      }

      this.actor.gotoAndStop('424')
    })

    it('should navigate to the next clip', function (done) {
      this.actor.onClipComplete = clip => {
        clip.next.should.equal('210')

        this.actor.onFrameChange = ({clipIndex, frame}) => {
          clipIndex.clip.should.equal('210')
          clipIndex.index.should.equal(0)
          done()
        }
      }

      this.actor.gotoAndStop('424')
      this.update(3)
    })
  })
})
