/**
 * @typedef FrameObject
 * @memberOf Actor
 * @instance
 * @type {object}
 * @property {PIXI.Texture} texture - The {@link PIXI.Texture} of the frame
 * @property {number} time - the duration of the frame in ms
 */

/**
 * @typedef ClipIndex
 * @memberOf Actor
 * @instance
 * @type {object}
 * @property {string} clip - The name of the clip
 * @property {number} index - The index of the frame
 */

/**
 * @typedef Clip
 * @memberOf Actor
 * @type {object}
 * @property {string} name - The name that will be used to reference this clip. If the name is the same as another clip previously added to this Actor, this clip will overwrite it. This doesn't, strictly speaking, *have* to be a string but to be safe it should be. If this is a number (it never should be), it might cause a stack overflow.
 * @property {number[]} frames - A sequence of frames to play.
 * @property {number | string | ClipIndex} [next] - The optional frame or clip to play after this one finishes. If this is not provided, the Actor will go to the default frame or animation when the clip completes. Set this to the clip's name to achieve a looping animation.
 * @property {number} [speed] - The relative speed at which each frame is played. The default is 1. A value of 2 means that every frame will take half as long. Negative values are currently not tested, so use at your own risk.
 */

/**
 * An Actor is a powerful way to define frames and animations that include those frames.
 * Initialize the `Actor` with some `FrameObject`s or `Texture`s just like you would with an `AnimatedSprite`
 * ```
 * const actor = new PIXI.Actor(
 *     {texture: PIXI.Texture.from('0.png'), time: 0.2},
 *     {texture: PIXI.Texture.from('1.png'), time: 0.5},
 *     {texture: PIXI.Texture.from('2.png'), time: 0.15},
 *     {texture: PIXI.Texture.from('3.png'), time: 0.9},
 *     {texture: PIXI.Texture.from('4.png'), time: 2.5}
 * )
 * ```
 *
 * Add some clips
 * ```
 * actor.addClips(
 *     {name: 'anim0', frames: [0, 4, 2], next: 'anim0'}, // loop an animation by setting next to itself
 *     {name: 'anim1', frames: [4, 1, 1, 2], next: 2}, // go to a frame after clip completes
 *     {name: 'anim2', frames: [2, 3, 0, 1], next: {clip: 'anim1', index: 1}}, // go to a specific frame in a clip
 *     {name: 'anim3', frames: [3], speed: 2}, // play the clip at a specific speed
 * )
 * ```
 *
 * Play something!
 * ```
 * actor.gotoAndPlay('anim0') // play a clip at the beginning
 * actor.gotoAndPlay('anim1', 2) // play a clip at a specific frame
 * actor.gotoAndPlay(3) // like AnimatedSprite, you can play just a plain ol' frame
 * ```
 *
 *
 * @param {PIXI.Texture[]|FrameObject[]} textures - an array of {@link PIXI.Texture} or frame objects that make up the animation
 * @param {boolean} [autoUpdate=true] - Whether to use PIXI.ticker.shared to auto update animation time.
 * @global
 * @class Actor
 * @extends PIXI.Sprite
 */
export default class Actor extends PIXI.Sprite {
  constructor (textures, ticker = PIXI.ticker.shared) {
    super(textures[0].texture || textures[0])

    /**
     * The name to use for the reserved clip.
     * This clip is used internally to play all the frames sequentially like a PIXI.extras.AnimatedSprite.
     *
     * @type {string}
     * @private
     */
    this._reservedClipName = '__RESERVED__'

    /**
     * @private
     */
    this._textures = null

    /**
     * @private
     */
    this._durations = null

    /**
     * How long the current frame has been animating.
     * @type {Number}
     * @default 0
     * @private
     */
    this._currentFrameElapsed = 0

    /**
     * @private
     * @type {object}
     * @default {}
     */
    this._clips = {}

    /**
     * @private
     */
    this._clipIndex = this.clipIndex(0)

    /**
     * The ticker to add the event listener to.
     * @type {PIXI.ticker.Ticker}
     * @default PIXI.ticker.shared
     * @private
     */
    this._ticker = ticker

    /**
     * The speed that the Actor will play at. Higher is faster, lower is slower
     * Prefer achieving reverse animation by defining a clip and not negative animation speed.
     *
     * @member {number} animationSpeed
     * @memberOf Actor
     * @instance
     * @instance
     * @default 1
     */
    this.animationSpeed = 1

    /**
     * Whether or not the Actor repeats after playing
     *
     * @member {boolean}
     * @memberOf Actor
     * @instance
     * @default true
     * @private
     */
    this._loop = true

    /**
     * Function to call when an Actor finishes playing
     *
     * @member {Function} onComplete
     * @memberOf Actor
     * @instance
     * @deprecated
     * @see Actor.onClipComplete
     */
    this.onComplete = null

    /**
     * Function to call when a clip finishes playing.
     *
     * @member {Function} onClipComplete
     * @memberOf Actor
     * @instance
     */
    this.onClipComplete = null

    /**
     * Function to call when an Actor goes to a frame.
     * Is called regardless of whether or not the texture is updated.
     *
     * @member {Function} onFrameChange
     * @memberOf Actor
     * @instance
     */
    this.onFrameChange = null

    /**
     * Function to call when 'loop' is true, and an Actor is played and loops around to start again
     *
     * @member {Function} onLoop
     * @memberOf Actor
     * @instance
     */
    this.onLoop = null

    /**
     * Elapsed time since animation has been started, used internally to display current texture
     *
     * @member {number}
     * @private
     */
    this._currentTime = 0

    /**
     * Indicates if the Actor is currently playing
     *
     * @member {boolean}
     * @private
     */
    this._playing = false

    /**
     * The default frame or clip to go to when there is no explicitly defined next.
     *
     * @type {ClipIndex}
     * @private
     */
    this._default = this.clipIndex(this._reservedClipName)

    this.textures = textures
  }

  /**
   * Returns a ClipIndex indicating the specific frame and clip to play.
   *
   * @param  {string | number | ClipIndex} frameOrClip - A string for the first frame of the named clip, a number for a specific frame, or a ClipIndex.
   * @param  {number} [index] - the index of the given clip to play
   * @return {ClipIndex} A ClipIndex with the given data
   * @private
   * @memberOf Actor
   * @instance
   */
  clipIndex (frameOrClip, index = 0) {
    if (typeof frameOrClip === 'object') {
      return frameOrClip
    } else if (typeof frameOrClip === 'string') {
      return {clip: frameOrClip, index}
    } else {
      return {clip: this._reservedClipName, index: frameOrClip}
    }
  }

  /**
   * Gets the frame from the current clip index or from a specified spot.
   *
   * @param  {string | number | ClipIndex} [frameOrClip]
   * @param  {number} [index]
   * @return {number}
   * @memberOf Actor
   * @instance
   */
  frame (frameOrClip = this._clipIndex, index = 0) {
    const clipIndex = this.clipIndex(frameOrClip, index)

    return this._clips[clipIndex.clip].frames[clipIndex.index]
  }

  /**
   * Gets the next frame from the current clip index or from a specified spot.
   *
   * @param  {string | number | ClipIndex} [frameOrClip]
   * @param  {number} [index]
   * @return {number}
   * @memberOf Actor
   * @instance
   */
  nextFrame (frameOrClip = this._clipIndex, index = 0) {
    return this.frame(this.nextClipIndex(frameOrClip, index))
  }

  /**
   * Gets the clip index of the next frame from the current clip index or from a specified spot.
   *
   * @param  {string | number | ClipIndex} [frameOrClip]
   * @param  {number} [index]
   * @return {ClipIndex}
   * @memberOf Actor
   * @instance
   */
  nextClipIndex (frameOrClip = this._clipIndex, index = 0) {
    const clipIndex = this.clipIndex(frameOrClip, index)
    const clip = this._clips[clipIndex.clip]

    if (clipIndex.index === clip.frames.length - 1) {
      return clip.next ? this.clipIndex(clip.next) : this._default
    } else {
      return {clip: clipIndex.clip, index: clipIndex.index + 1}
    }
  }

  /**
   * Stops the Actor
   *
   * @memberOf Actor
   * @instance
   */
  stop () {
    if (this._playing) {
      this._playing = false
      this._ticker.remove(this.update, this)
    }
  }

  /**
   * Plays the Actor.
   *
   * @memberOf Actor
   * @instance
   */
  play () {
    if (!this._playing) {
      this._playing = true
      this._ticker.add(this.update, this, PIXI.UPDATE_PRIORITY.HIGH)
    }
  }

  /**
   * Goes to a specific frame or clip.
   *
   * @param  {number | string} frameOrClip - frame index or clip name to go to
   * @param  {number} [index]
   * @memberOf Actor
   * @instance
   */
  goto (frameOrClip, index = 0) {
    const previousFrame = this.currentFrame

    this._clipIndex = this.clipIndex(frameOrClip, index)

    this._invoke(this.onFrameChange, {frame: this.currentFrame, clipIndex: this._clipIndex})
    if (previousFrame !== this.currentFrame) {
      this.updateTexture()
    }
  }

  /**
   * Stops the Actor and goes to a specific frame or clip
   *
   * @param {number | string} frameOrClip - frame index or clip name to stop at
   * @param  {number} [index]
   * @memberOf Actor
   * @instance
   */
  gotoAndStop (frameOrClip, index = 0) {
    this.stop()
    this.goto(frameOrClip, index)
  }

  /**
   * Goes to a specific frame or clip and begins playing the Actor
   *
   * @param {number | string} frameOrClip - frame index or clip name to start at
   * @param  {number} [index]
   * @memberOf Actor
   * @instance
   */
  gotoAndPlay (frameOrClip, index = 0) {
    this.goto(frameOrClip, index)
    this.play()
  }

  /**
   * Adds an arbitrary number of clips to the Actor.
   * The order that the clips will be added is not deterministic, so if there are conflicting clips, it is undefined which one will remain.
   * @param {...Clip} clips - the clips to add
   * @memberOf Actor
   * @instance
   */
  addClips (...clips) {
    clips.forEach(({name, frames, next, speed}) => {
      this._clips[name] = {
        frames,
        next,
        speed: speed || 1
      }
    })
  }

  /**
   * Removes clips from the Actor. If you want to replace a clip, simply add one with the same name.
   *
   * @param  {...string} clipNames - the names of the clips to remove
   * @memberOf Actor
   * @instance
   */
  removeClips (...clipNames) {
    clipNames.forEach(name => delete this._clips[name])
  }

  /**
   * Gets the clip index based on time.
   * @param  {number} deltaTime - time since last tick
   * @return {@link ClipIndex}
   * @private
   * @memberOf Actor
   * @instance
   */
  clipIndexAfterWait (deltaTime) {
    let current, next, duration
    const loop = (elapsed, c) => {
      current = c
      next = this.nextClipIndex(current)
      duration = this._durations[this.frame(next)] / this.animationSpeed / this._clips[current.clip].speed - this._currentFrameElapsed

      if (current.clip !== next.clip) {
        this._invoke(this.onClipComplete, this._clips[current.clip])
      } else if (current.index !== next.index - 1) {
        this._invoke(this.onLoop, this._clips[current.clip])
      }

      if (duration <= elapsed) {
        this._currentFrameElapsed = 0
        loop(elapsed - duration, next)
      } else {
        this._currentFrameElapsed += elapsed
      }
    }

    loop(deltaTime, this._clipIndex)

    return current
  }

  /**
   * Updates the object transform for rendering.
   *
   * @param {number} deltaTime - Time since last tick.
   * @private
   * @memberOf Actor
   * @instance
   */
  update (deltaTime) {
    this.goto(this.clipIndexAfterWait(deltaTime))
  }

  /**
   * Updates the displayed texture to match the current frame index
   *
   * @private
   * @memberOf Actor
   * @instance
   */
  updateTexture () {
    this._texture = this._textures[this.currentFrame]
    this._textureID = -1
    this.cachedTint = 0xFFFFFF

    this._invoke(this.onFrameChange, {frame: this.currentFrame, clip: this.currentClip, frameIndex: this._clipIndex})
  }

  /**
   * Stops the Actor and destroys it
   *
   * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
   *  have been set to that value
   * @param {boolean} [options.children=false] - if set to true, all the children will have their destroy
   *      method called as well. 'options' will be passed on to those calls.
   * @param {boolean} [options.texture=false] - Should it destroy the current texture of the sprite as well
   * @param {boolean} [options.baseTexture=false] - Should it destroy the base texture of the sprite as well
   * @memberOf Actor
   * @instance
   */
  destroy (options) {
    this.stop()
    super.destroy(options)
  }

  /**
   * Util method for invoking callbacks.
   * @param  {Function}  callback the function to call
   * @param  {...*} args    the args to pass
   * @private
   * @memberOf Actor
   * @instance
   */
  _invoke (callback, ...args) {
    if (callback) {
      callback(...args) // eslint-disable-line
    }
  }

  /**
   * Removes the update listener from the ticker.
   * @private
   * @memberOf Actor
   * @instance
   */
  _removeListener () {
    if (this._ticker) {
      this._ticker.remove(this.update, this)
    }
  }

  /**
   * Adds the update listener to the ticker.
   * @private
   * @memberOf Actor
   * @instance
   */
  _addListener () {
    if (this._ticker) {
      this._ticker.add(this.update, this)
    }
  }

  /**
   * A short hand way of creating an Actor from an array of frame ids
   *
   * @static
   * @param {string[]} frames - The array of frames ids the Actor will use as its texture frames
   * @return {Actor} The new actor with the specified frames.
   * @memberOf Actor
   * @static
   */
  static fromFrames (frames) {
    return new Actor(frames.map(frame => PIXI.Texture.fromFrame(frame)))
  }

  /**
   * A short hand way of creating an Actor from an array of image ids
   *
   * @static
   * @param {string[]} images - the array of image urls the Actor will use as its texture frames
   * @return {Actor} The new actor with the specified images as frames.
   * @memberOf Actor
   * @static
   */
  static fromImages (images) {
    return new Actor(images.map(image => PIXI.Texture.fromImage(image)))
  }

  /**
   * The ticker to use to update the actor.
   * @member {PIXI.ticker.Ticker}
   * @default PIXI.ticker.shared
   * @memberOf Actor
   * @instance
   */
  get ticker () {
    return this.ticker
  }

  set ticker (value) {
    this._removeListener()
    this._ticker = value
    this._addListener()
  }

  /**
   * This clip is used internally to play all the frames sequentially like a PIXI.extras.AnimatedSprite.
   *
   * @member {Clip}
   * @readonly
   * @memberOf Actor
   * @instance
   */
  get reservedClip () {
    return this._clips[this._reservedClipName]
  }

  /**
   * The name to use for the reserved clip.
   *
   * @member {string}
   * @memberOf Actor
   * @instance
   */
  get reservedClipName () {
    return this._reservedClipName
  }

  set reservedClipName (value) {
    if (this._default.clip === this._reservedClipName) this._default.clip = value
    this._clips[value] = this._clips[this._reservedClipName]
    delete this._clips[this._reservedClipName]

    this._reservedClipName = value
  }

  /**
   * The default frame or clip to go to when there is no explicitly defined next.
   *
   * @member {number | string | ClipIndex}
   * @memberOf Actor
   * @instance
   */
  get default () {
    return this._default
  }

  set default (value) {
    this._default = this.clipIndex(value)
  }

  /**
   * Indicates if the Actor is currently playing
   *
   * @member {boolean}
   * @memberOf Actor
   * @instance
   * @readonly
   */
  get playing () {
    return this._playing
  }

  /**
   * The clips that this Actor contains.
   *
   * @readonly
   * @member {object}
   * @memberOf Actor
   * @instance
   * @default {}
   */
  get clips () {
    return this._clips
  }

  /**
   * totalFrames is the total number of frames in the Actor. This is the same as number of textures
   * assigned to the Actor.
   *
   * @readonly
   * @member {number}
   * @memberOf Actor
   * @instance
   * @default 0
   */
  get totalFrames () {
    return this._textures.length
  }

  /**
   * The array of textures used for this Actor
   *
   * @member {PIXI.Texture[]}
   * @memberOf Actor
   * @instance
   */
  get textures () {
    return this._textures
  }

  set textures (value) { // eslint-disable-line require-jsdoc
    this._textures = []
    this._durations = []

    this.addClips({name: this._reservedClipName, frames: value.map((e, i) => i), next: this._loop ? this._reservedClipName : undefined})

    value.forEach(e => {
      this._textures.push(e.texture || e)
      this._durations.push(e.time || 1)
    })

    this.gotoAndStop(this._default)
    this.updateTexture()
  }

  /**
   * Whether or not the Actor repeats after playing
   *
   * @member {boolean}
   * @memberOf Actor
   * @instance
   * @default true
   */
  get loop () {
    return this._loop
  }

  set loop (value) {
    this._clips[this._reservedClipName].next = value ? this._reservedClipName : undefined
    this._loop = value
  }

  /**
   * The Actors current frame number
   *
   * @member {number}
   * @memberOf Actor
   * @instance
   * @readonly
   */
  get currentFrame () {
    return this.frame()
  }

  /**
   * The actor's current clip.
   *
   * @member {Clip}
   * @memberOf Actor
   * @instance
   * @readonly
   */
  get currentClip () {
    return this._clips[this._clipIndex.clip]
  }
}
