const path = require('path')
const baseConfig = require('./webpack.base.config')
const {merge} = require('lodash')

module.exports = merge(baseConfig, {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, '..', 'dist'),
    filename: 'pixi-actor.js'
  },
  context: path.resolve(__dirname, '..'),
  devtool: 'inline-source-map'
})
