import Actor from './src/Actor'

export default Actor

if (!PIXI) {
  console.log('Please load pixi.js before loading pixi-actor!') // eslint-disable-line
} else {
  PIXI.Actor = Actor
}
