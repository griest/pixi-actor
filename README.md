pixi-actor
==========

[![npm](https://img.shields.io/npm/v/pixi-actor.svg)](https://www.npmjs.com/package/pixi-actor)
[![pipeline status](https://gitlab.com/griest/pixi-actor/badges/master/pipeline.svg)](https://gitlab.com/griest/pixi-actor/commits/master)
[![npm](https://img.shields.io/npm/l/pixi-actor.svg)](https://gitlab.com/griest/pixi-actor/LICENSE)

Like [`PIXI.extras.AnimatedSprite`](http://pixijs.download/release/docs/PIXI.extras.AnimatedSprite.html) but with more features!

Installation
------------
#### NPM
```
npm install pixi-actor --save
```
#### Yarn
```
yarn add pixi-actor
```
#### CDN
```
<script src="https://unpkg.com/pixi-actor/dist/pixi-actor.js"></script>
```

Usage
-----
Import `pixi.js` *before* you import `pixi-actor`
```
import 'pixi.js'
import 'pixi-actor' // this plugin modifies the global PIXI object
```
*OR*
```
import 'pixi.js'
import Actor from 'pixi-actor' // it also exports Actor directly, take your pick
```

Initialize the `Actor` with some `FrameObject`s or `Texture`s just like you would with an `AnimatedSprite`
```
const actor = new PIXI.Actor(
    {texture: PIXI.Texture.from('0.png'), time: 0.2},
    {texture: PIXI.Texture.from('1.png'), time: 0.5},
    {texture: PIXI.Texture.from('2.png'), time: 0.15},
    {texture: PIXI.Texture.from('3.png'), time: 0.9},
    {texture: PIXI.Texture.from('4.png'), time: 2.5}
)
```

Add some clips
```
actor.addClips(
    {name: 'anim0', frames: [0, 4, 2], next: 'anim0'}, // loop an animation by setting next to itself
    {name: 'anim1', frames: [4, 1, 1, 2], next: 2}, // go to a frame after clip completes
    {name: 'anim2', frames: [2, 3, 0, 1], next: {clip: 'anim1', index: 1}}, // go to a specific frame in a clip
    {name: 'anim3', frames: [3], speed: 2}, // play the clip at a specific speed
)
```

Play something!
```
actor.gotoAndPlay('anim0') // play a clip at the beginning
actor.gotoAndPlay('anim1', 2) // play a clip at a specific frame
actor.gotoAndPlay(3) // like AnimatedSprite, you can play just a plain ol' frame
```

Documentation
-------------

Check out the [API docs](https://griest.gitlab.io/pixi-actor/docs)

Reports
-------

- [Unit Tests](https://griest.gitlab.io/pixi-actor/reports/unit)
- [Test Coverage](https://griest.gitlab.io/pixi-actor/reports/coverage)
